package br.edu.infnet.order.resources.dto;

import java.util.List;

public class OrderDTO {
	
	private Integer orderId;
	private Integer clientID;
	private List <ProductDTO> products;

}
