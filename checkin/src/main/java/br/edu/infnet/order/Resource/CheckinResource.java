package br.edu.infnet.order.Resource;

import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.RestTemplate;

import br.edu.infnet.order.Resource.dto.CheckinDTO;
import br.edu.infnet.order.Resource.dto.CheckinResponseDTO;
import br.edu.infnet.order.Resource.dto.ClientDTO;
import br.edu.infnet.order.Resource.dto.ConsumacaoDTO;
import br.edu.infnet.order.clients.ConsumacaoClient;
import ch.qos.logback.classic.Logger;

@RestController
@RequestMapping("/checkins")
public class CheckinResource {
	
	//colocar os logs 17:25 https://drive.google.com/file/d/14Ixa8WlskWiAgJPKhtn0mR7cPLoO6Co5/view
	
	private static Logger log = (Logger) LoggerFactory.getLogger(CheckinResource.class);
	
	@Autowired
	private RestTemplate restemplate;  
	
	@Value("${client.endpoint.url}")
	private String clientApiUrl;
	
	@Autowired
	private ConsumacaoClient consumacaoClient;
	
	
	//https://drive.google.com/file/d/1IOtRA4eczjMXzn5ptDuRe9wvVkoIIz87/view 1:08
	@PostMapping
	public CheckinResponseDTO efetuaCheckin(@RequestBody CheckinDTO checkinDTO) {

		log.info("@@@ solicitação post requestBody CheckinDTO checkinDTO");
		log.info("@@@ solicitação para checkin com a seguinte informação: {}",checkinDTO);
		if(log.isDebugEnabled()) {
			log.debug("### aqui é para aparecer somente quando for um log debug.");
		}
		System.out.println(checkinDTO);
		
		  ClientDTO clientDTO = restemplate.getForObject(clientApiUrl+checkinDTO.getClientID(),
		  ClientDTO.class);

		  log.info("@@@ terminou a chamada rest para API cliente DTO, informação do cliente DTO é a seguinte: {}",clientDTO);
		  
		 List<ConsumacaoDTO> consumacoes = consumacaoClient.getConsumacoes();

		 log.info("@@@@ clase CheckinResource, informação consumacoes é : {}",consumacoes);
		 
		 return new CheckinResponseDTO( checkinDTO.getCheckinId() ,clientDTO, consumacoes);
			
		 
	}
}