package br.edu.infnet.order.Resource.dto;

public class ClientDTO {
		

		private Long codigo;
		private String email;
		private String nome;
		private String endereco;
		
		public ClientDTO() {
			// TODO Auto-generated constructor stub
		}
		public Long getCodigo() {
		return codigo;
			
		}
		public void setCodigo(Long codigo) {
			this.codigo = codigo;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getNome() {
			return nome;
		}
		public void setNome(String nome) {
			this.nome = nome;
		}
		public String getEndereco() {
			return endereco;
		}
		public void setEndereco(String endereco) {
			this.endereco = endereco;
		}
		public ClientDTO(Long codigo, String email, String nome, String endereco) {
			super();
			this.codigo = codigo;
			this.email = email;
			this.nome = nome;
			this.endereco = endereco;
		}
		@Override
		public String toString() {
			return "ClientDTO [codigo=" + codigo + ", email=" + email + ", nome=" + nome + ", endereco=" + endereco
					+ "]";
		}	
}
