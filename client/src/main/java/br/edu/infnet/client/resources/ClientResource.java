package br.edu.infnet.client.resources;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.infnet.client.model.entities.Client;
import br.edu.infnet.client.model.services.ClientService;
import ch.qos.logback.classic.Logger;

@RestController
@RequestMapping("/clients")
public class ClientResource {
	private static Logger log = (Logger) LoggerFactory.getLogger(ClientResource.class);
	
	@Autowired
	private ClientService clientService;
	
	//https://drive.google.com/file/d/1IOtRA4eczjMXzn5ptDuRe9wvVkoIIz87/view 55:02
	@GetMapping("/{codigo}")
	public Client getClient(@PathVariable Long codigo) {
		
		log.info(" classe ClientResource, código carregado do url é: {} " ,codigo);

		if(log.isDebugEnabled()) {
			log.debug(" aqui é para aparecer somente quando for um log debug."+" classe ClientResource, código carregado do url é: {} " ,codigo);
		}
		return clientService.getByCodigo(codigo);
		
	}
}