package br.edu.infnet.client.model.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.infnet.client.model.entities.Client;
import br.edu.infnet.client.model.repository.ClientRepository;

@Service
public class ClientService {
	
	@Autowired
	private ClientRepository clientRepository;
	
	public Client getByCodigo(Long codigo) {
		return clientRepository.findById(codigo).get();
	}

}
