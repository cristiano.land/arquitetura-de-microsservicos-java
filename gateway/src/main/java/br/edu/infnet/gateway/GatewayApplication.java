package br.edu.infnet.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GatewayApplication {

	@Bean
	public RouteLocator routes(RouteLocatorBuilder builder) {
		return builder.routes()
		//definições do proxy
		.route(p -> p
				.path("/clients/**")
				.uri("http://localhost:8799")
				)
		.route(p -> p
				.path("/consumacoes/**")
				.uri("http://localhost:8989")
				)	
		.route(p -> p
				.path("/eureka/apps/**")
				.uri("http://localhost:8761")
				)
		
		.build();	
		
	}
	
	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}

}
