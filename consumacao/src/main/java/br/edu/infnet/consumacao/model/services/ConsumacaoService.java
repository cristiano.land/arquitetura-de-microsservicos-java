package br.edu.infnet.consumacao.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.infnet.consumacao.model.entities.Consumacao;
import br.edu.infnet.consumacao.model.repository.ConsumacaoRepository;

@Service
public class ConsumacaoService {
	
	@Autowired
	private ConsumacaoRepository consumacaoRepository;
	
	public List<Consumacao> getAll(){
		return (List<Consumacao>) consumacaoRepository.findAll();
	}

}
